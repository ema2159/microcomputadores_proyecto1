# AVL Tree data structure

This is a proyect for IE-0724 Programming laboratory and microcomputers course, Costa Rica, UCR

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

* Python 3
* Matplotlib
* Pandas
* Numpy

### Executing

This proyect has a makefile. It is on the test/ directory and it has the following targets:

* all: clear, compile, run and plot the main program for all id node numbers
* compile: compiles all the unit test and the main file
* clear: elimina todos los archivos .o
* utest $(TEST): runs the $(TEST) unit test. $(TEST) variable must be passed as an argument 
```
Example: make utest TEST=1
```
* execmain $(NUM): runs the main program. $(NUM) variable must be passed as an argument 
```
Example: make execmain NUM=1000
```
* exall: runs the main program on all the id files
* plot: generates a csv file with the execution time vs number of id nodes and plots it using Python

## Tests
For testing the data structure created, four unit tests were implemented. 

* Unit test 1: created a tree given an array of node pointers using the sorting and building algorithm. Then inserted nodes to test each of the 4 AVL rotations, printing the tree in each step.
* Unit test 2: created a tree given an array of node pointers using the sorting and building algorithm. Then tested 3 delete cases, then deleted all nodes. Then inserted a node when the tree was empty
* Unit test 3: read a .txt file with names and ids, inserted 5 types of possible errors. Built an avl tree ignoring lines with errors.
* Unit test 4: built a tree using avl insertions. Tried to insert a NULL node and to delete a non existing node giving an error message and not crashing the program. Then deleted all nodes from the tree.

## Built With

* C++

## Author

* **Emmanuel Bustos T.** - *Git* - [ema2159](https://gitlab.com/ema2159/)
