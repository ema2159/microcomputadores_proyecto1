#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <math.h> 
#include "string.h"

using namespace std;

class node{
	public:
	//Node class atributes
	unsigned int id;
	string name;
	node* lc;
	node* rc;
	short int hl;
	short int hr;
	//Node class costructors
	node();
	node(unsigned int id, string name, node* lc, node* rc);
	//Node class methods
	void print_data();
	void operator~();
	//Node class destructor
	virtual ~node();
};
#endif /* NODE_H */
