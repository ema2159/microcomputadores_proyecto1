#ifndef AVL_TREE_H
#define AVL_TREE_H
#include <iostream>
#include <math.h> 
#include "string.h"
#include "../src/node.cpp"
#include <stack>
#include <algorithm>

using namespace std;

class avl_tree{
	public:
	//AVL Tree class atributes
	node* root;
	stack <node*> search_stack;
	//AVL Tree class costructors
	avl_tree();
	avl_tree(node* node_array[], int size);
	avl_tree(node* root);
	//AVL Tree class methods
	void insert(node);
	void remove(node);
	int get_size();
	int get_max_height(); 
	void printTree(node* n);
	void printSubtree(node* n, const string& prefix);
	void swap(node* a, node* b);
	int partition (node* arr[], int low, int high);
	void quickSort(node* arr[], int low, int high);
	void printNodeArray(node* arr[], int size);
	void build_tree(node* curr[], int begin, int end);
	void deleteNode(node* deletedNode);
	void deleteNode(unsigned int deletedNumber);
	void deleteHeightUpdate(node* in);
	void insertHeightUpdate(node* in);
	void insertNode(node* insert);
	void avl_insert(node* insert);
	void avl_delete(node* deleted);
	void avl_delete(unsigned int deletedNumber);
	//void insertNode(unsigned int insertedNumber, string name);
	void showstack();
	void rotationRR(node* initial);
	void rotationLL(node* initial);
	void rotationRL(node* initial);
	void rotationLR(node* initial);
	void balance();
	void printstack(stack<node*> stck);
	node* minFromMax(node* root);
	node* get_maxnode();
	node* get_minnode();
	node* search(node* initial, unsigned int find);
	node* searchFromRoot(unsigned int find);
	void operator~();
	//AVL Tree class destructor
	virtual ~avl_tree();
};
#endif /* AVL_TREE_H */
