#include "../src/avl_tree.cpp"
#include "string.h"

using namespace std;

int main(int argc, char** argv) {
	string nombre = "Emmanuel Bustos T.";
	unsigned int id = 116970136;
	node* node_array[9];
	node node0(41, nombre, NULL, NULL);
	node node8(8, nombre, NULL, NULL); 
	node node9(9, nombre, NULL, NULL); 
	node node6(6, nombre, NULL, NULL); 
	node nodex(7, nombre, NULL, NULL); 
	node nodey(39, nombre, NULL, NULL); 
	node node11(10, nombre, NULL, NULL); node_array[0] = &node11;
	node node12(30, nombre, NULL, NULL); node_array[1] = &node12;
	node node1(20, nombre, NULL, NULL); node_array[2] = &node1;
	node node21(50, nombre, NULL, NULL); node_array[3] = &node21;
	node node22(70, nombre, NULL, NULL); node_array[4] = &node22;
	node node2(60, nombre, NULL, NULL); node_array[5] = &node2;
	node node3(80, nombre, NULL, NULL); node_array[6] = &node3;
	node root(40, nombre, NULL, NULL); node_array[7] = &root;
	node node31(90, nombre, NULL, NULL); node_array[8] = &node31;
	int size = 9;
	//Build tree using sort and build algorithm
	avl_tree tree(node_array, size);
	//Print initial tree
	~tree;
	//Test RR Rotation
	tree.avl_insert(&node0); //Insert 41
	~tree;
	//Test RL Rotation
	tree.avl_insert(&nodey); //Insert 39
	~tree;
	//Test LL Rotation
	tree.avl_insert(&nodex); //Insert 7
	~tree;
	//Test LR Rotation
	tree.avl_insert(&node8); //Insert 8
	~tree;
	tree.avl_insert(&node6); //Insert 6
	~tree;
	tree.avl_insert(&node9); //Insert 9
	~tree;
	tree.avl_delete(&nodex);
	~tree;
	tree.avl_delete(node_array[4]);
	~tree;
	tree.avl_delete(node_array[3]);
	~tree;
}
