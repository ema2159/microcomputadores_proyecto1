#include "../src/avl_tree.cpp"
#include "string.h"
#include <fstream>
#include <vector>

using namespace std;

int main(int argc, char** argv) {
	avl_tree id_tree(NULL);
	vector <node*> insert_vector;
	ifstream file("../misc/lista_10_errores.txt");
	string line, name, id_str;
	unsigned long int id, linecount;
	short int commapos;
	linecount = 1;
	while(getline(file, line))
	{	
		commapos = line.find(",");
		name = line.substr(0, commapos);
		id_str = line.substr(commapos+2, line.length());
		if (name.compare("") == 0)
		{
			cout << "ERROR N°1: línea " << linecount << " sin nombre  \n \n";
		}
		else {
			if (id_str.compare("") == 0)
			{
				cout << "ERROR N°2: línea " << linecount << " sin cédula \n \n";
			}
			else if (id_str.find_first_not_of("1234567890")!=std::string::npos)
			{
				cout << "ERROR N°3: caracteres no numéricos en la cédula de la línea " << linecount << " \n \n";
			}
			else
			{				
				id = stoul(id_str,nullptr, 10);
				if (id < 100000000)
				{
					cout << "ERROR N°4: cédula inválida en la línea " << linecount << ", es menor a 100000000 \n \n";
				}
				else if (id > 999999999)
				{
					cout << "ERROR N°5: cédula inválida en la línea " << linecount << ", es mayor a 999999999 \n \n";
				}
				else
				{
					insert_vector.emplace_back(new node);
					insert_vector.back()->id = id;
					insert_vector.back()->name = name;
					id_tree.avl_insert(insert_vector.back()); 
				}
			} 
		}
		linecount++;
	}
	~id_tree;
}
