#include "../src/avl_tree.cpp"
#include "string.h"
#include <fstream>
#include <vector>
#include <chrono>

using namespace std;

int main(int argc, char** argv) {
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	avl_tree id_tree(NULL);
	vector <node*> insert_vector;
	string filenum(argv[1]);
	string file_name = "../misc/lista_" + filenum + ".txt";
	ifstream file(file_name);
	if (!file.good())
	{
		cout << filenum << "\n";
		cout << "ERROR, archivo inválido. Vuelva a ejecutar el programa con un archivo válido \n \n";
		return 0;
	}
	cout << "Cargando: " << file_name << "\n \n";
	string line, name, id_str;
	unsigned long int id, linecount;
	short int commapos;
	linecount = 1;
	while(getline(file, line))
	{	
		commapos = line.find(",");
		name = line.substr(0, commapos);
		id_str = line.substr(commapos+2, line.length());
		if (name.compare("") == 0)
		{
			cout << "ERROR N°1: línea " << linecount << " sin nombre  \n \n";
		}
		else {
			if (id_str.compare("") == 0)
			{
				cout << "ERROR N°2: línea " << linecount << " sin cédula \n \n";
			}
			else if (id_str.find_first_not_of("1234567890")!=std::string::npos)
			{
				cout << "ERROR N°3: caracteres no numéricos en la cédula de la línea " << linecount << " \n \n";
			}
			else
			{				
				id = stoul(id_str,nullptr, 10);
				if (id < 100000000)
				{
					cout << "ERROR N°4: cédula inválida en la línea " << linecount << ", " << id << " es menor a 100000000 \n \n";
				}
				else if (id > 999999999)
				{
					cout << "ERROR N°5: cédula inválida en la línea " << linecount << ", " << id << " es mayor a 999999999 \n \n";
				}
				else
				{
					insert_vector.emplace_back(new node);
					insert_vector.back()->id = id;
					insert_vector.back()->name = name;
					id_tree.avl_insert(insert_vector.back()); 
				}
			} 
		}
		linecount++;
	}
	ofstream outputcsv, time_exec;
	outputcsv.open("../misc/output/output" + filenum + ".csv");
	node* min = id_tree.get_minnode();
	node* max = id_tree.get_maxnode();
	cout << "La cédula mínima es: " << min->id << "\n";
	cout << "La cédula máxima es: " << max->id << "\n";
	outputcsv << "Cantidad de cédulas," << filenum << "\n";
	outputcsv << "La cédula mínima es," << min->id << "\n";
	outputcsv << "La cédula máxima es," << max->id << "\n";
	outputcsv.close();
	end = std::chrono::system_clock::now();
	int exec_time = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	cout << "Tiempo de ejecución: " << exec_time << " us\n";
	time_exec.open("../misc/data/running_times" + filenum + ".csv");	
	time_exec << filenum << "," << exec_time << "\n";
	time_exec.close();
}
