#include "../src/avl_tree.cpp"
#include "string.h"

using namespace std;

int main(int argc, char** argv) {
	string nombre = "Emmanuel Bustos T.";
	unsigned int id = 116970136;
	node* node_array[9];
	node node0(41, nombre, NULL, NULL);
	node node8(8, nombre, NULL, NULL); 
	node node9(9, nombre, NULL, NULL); 
	node node6(29, nombre, NULL, NULL); 
	node nodex(28, nombre, NULL, NULL); 
	node nodey(39, nombre, NULL, NULL); 
	node node11(10, nombre, NULL, NULL); node_array[0] = &node11;
	node node12(30, nombre, NULL, NULL); node_array[1] = &node12;
	node node1(20, nombre, NULL, NULL); node_array[2] = &node1;
	node node21(50, nombre, NULL, NULL); node_array[3] = &node21;
	node node22(70, nombre, NULL, NULL); node_array[4] = &node22;
	node node2(60, nombre, NULL, NULL); node_array[5] = &node2;
	node node3(80, nombre, NULL, NULL); node_array[6] = &node3;
	node root(40, nombre, NULL, NULL); node_array[7] = &root;
	node node31(90, nombre, NULL, NULL); node_array[8] = &node31;
	int size = 9;
	//Build tree using sort and build algorithm
	avl_tree tree(node_array, size);
	//Print initial tree
	~tree;
	//Deleted root. Minimum element from right subtree is a leaf
	tree.avl_delete(node_array[4]);
	~tree;
	//Deleted node wich its minimum element from the right subtree has a right subtree
	tree.avl_delete(node_array[1]);
	~tree;
	//Deleted leaf
	tree.avl_delete(node_array[0]);
	~tree;
	//Delete other nodes
	tree.avl_delete(node_array[4]);
	~tree;
	tree.avl_delete(node_array[4]);
	~tree;
	tree.avl_delete(node_array[4]);
	~tree;
	tree.avl_delete(node_array[4]);
	~tree;
	tree.avl_delete(90);
	~tree;
	//Delete last tree element
	tree.avl_delete(30);
	~tree;
	//Inserted node when the tree is empty
	tree.avl_insert(&nodex);
	~tree;
}
