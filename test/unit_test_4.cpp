#include "../src/avl_tree.cpp"
#include "string.h"
#include <fstream>
#include <vector>

using namespace std;

int main(int argc, char** argv) {
	string nombre = "Emmanuel Bustos T.";
	unsigned int id = 116970136;
	int size = 9;
	node* node_array[size];
	node node0(41, nombre, NULL, NULL);
	node node8(8, nombre, NULL, NULL); 
	node node9(9, nombre, NULL, NULL); 
	node node6(6, nombre, NULL, NULL); 
	node nodex(7, nombre, NULL, NULL); 
	node nodey(39, nombre, NULL, NULL); 
	node node11(10, nombre, NULL, NULL); node_array[0] = &node11;
	node node12(30, nombre, NULL, NULL); node_array[1] = &node12;
	node node1(20, nombre, NULL, NULL); node_array[2] = &node1;
	node node21(50, nombre, NULL, NULL); node_array[3] = &node21;
	node node22(70, nombre, NULL, NULL); node_array[4] = &node22;
	node node2(60, nombre, NULL, NULL); node_array[5] = &node2;
	node node3(80, nombre, NULL, NULL); node_array[6] = &node3;
	node root(40, nombre, NULL, NULL); node_array[7] = &root;
	node node31(90, nombre, NULL, NULL); node_array[8] = &node31;
	//Build tree using sort and build algorithm
	avl_tree tree(NULL);
	//Build nodes with AVL insertions
	for (int i = 0; i < size; i++)
	{
		tree.avl_insert(node_array[i]);
	}
	//Print initial tree
	~tree;
	//Try to insert NULL
	tree.avl_insert(NULL);
  ~tree;
	//Try to delete a non existing node
	tree.avl_delete(300);
	~tree;
	while (tree.root != NULL)
	{
		tree.avl_delete(tree.root);
		~tree;
	}
}
