#! /usr/bin/python
import sys
import matplotlib.pyplot as plt
import pandas as pd
df = pd.read_csv("../misc/data/running_times.csv")
Asoc = df.N
MR = df.ExecutionT
plt.plot(Asoc, MR, 'ro')
plt.title('N VS Execution time')
plt.ylabel('Execution time (us)')
plt.xlabel('N')
plt.grid(True)
plt.show()
