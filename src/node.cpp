#include "../include/node.h"
#include "string.h"

///Node class constructor
node::node(){
	this->id = 0;
	this->name = "name";
	this->lc = NULL;
	this->rc = NULL;
	this->hl = 0;
	this->hr = 0;
}
///Node class destructor
node::~node(){
}

///Node class overloaded constructor 
node::node(unsigned int id, string name, node* lc, node* rc){
	this->id = id;
	this->name = name;
	this->lc = lc;
	this->rc = rc;
	this->hl = 0;
	this->hr = 0;
}

///Function prints the data of a node
void node::print_data(){
	cout << "Name: " << this->name <<  "\nID: " << this->id << "\nHL: "<< this->hl << "\nHR: "<< this->hr << "\n";
}
