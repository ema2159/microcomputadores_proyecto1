#include "../include/avl_tree.h"
#include "string.h"
#include <algorithm>

using namespace std;

//AVL_Tree class constructor
avl_tree::avl_tree()
{
	this->root = NULL;
	stack <node*> search_stack;
	this->search_stack = search_stack;
}
//AVL_Tree class destructor
avl_tree::~avl_tree()
{
}

//AVL_Tree class overloaded constructor 
avl_tree::avl_tree(node* root)
{
	this->root = root;
	stack <node*> search_stack;
	this->search_stack = search_stack;
}

//AVL_Tree class overloaded constructor 
avl_tree::avl_tree(node* node_array[], int size){
	quickSort(node_array, 0, size-1);
	this->root = node_array[(size-1)/2];
	build_tree(node_array, 0, size-1);
	stack <node*> search_stack;
	this->search_stack = search_stack;
}

//Function that prints the tree
void avl_tree::operator~(){
	printTree(this->root);
}

//Function that prints the subtree of a given node
void avl_tree::printTree(node* root)
{
	if (root == NULL)
	{
		return;
	}

	cout << root->id << endl;
	printSubtree(root, "");
	cout << endl;
}

//Suport function for printTree
void avl_tree::printSubtree(node* root, const string& prefix)
{
	if (root == NULL)
	{
		return;
	}

	bool hasLeft = (root->lc != NULL);
	bool hasRight = (root->rc != NULL);

	if (!hasLeft && !hasRight)
	{
		return;
	}

	cout << prefix;
	cout << ((hasLeft  && hasRight) ? "├── " : "");
	cout << ((!hasLeft && hasRight) ? "└── " : "");

	if (hasRight)
	{
		bool printStrand = (hasLeft && hasRight && (root->rc->rc != NULL || root->rc->lc != NULL));
		string newPrefix = prefix + (printStrand ? "│   " : "    ");
		cout << root->rc->id << "." << endl;
		printSubtree(root->rc, newPrefix);
	}

	if (hasLeft)
	{
		cout << (hasRight ? prefix : "") << "└── " << root->lc->id << endl;
		printSubtree(root->lc, prefix + "    ");
	}
}

//Support function that swaps two nodes in an array
void avl_tree::swap(node* a, node* b)
{
    node t = *a;
    *a = *b;
    *b = t;
}

//Support function for quicksort
int avl_tree::partition (node* arr[], int low, int high)
{
    int pivot = arr[high]->id;    // pivot
    int i = (low - 1);  // Index of smaller element
    for (int j = low; j <= high- 1; j++)
    {
			// If current element is smaller than or
			// equal to pivot
			if (arr[j]->id <= pivot)
			{
				i++;    // increment index of smaller element
				swap(arr[i], arr[j]);
			}
    }
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

//Function that sorts a given node array using quicksort
void avl_tree::quickSort(node* arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

//Function that prints a node array
void avl_tree::printNodeArray(node* arr[], int size)
{
	int i;
	for (i=0; i < size; i++)
	printf("%d ", arr[i]->id);
	printf("\n");
}

//Function that shows the tree search stack
void avl_tree::showstack()
{
    stack <node*> g = this->search_stack;
    cout << "Stack: ";
    while (!g.empty())
    {
        cout << g.top()->id << " (" << g.top()->hl << ", " << g.top()->hr << ") ";
        g.pop();
    }
    cout << '\n';
}

//Function that builds a tree recursively given an ordered node array
void avl_tree::build_tree(node* curr[], int begin, int end)
{	
	int center = begin + (end-begin)/2;
	int right = center + 1 + (end - center - 1)/2;
	int left = begin + (center-1-begin)/2;
	if (end-begin >= 3)
	{
		curr[center]->rc = curr[right];
		curr[center]->lc = curr[left];
		build_tree(curr, begin, center-1);
		build_tree(curr, center+1, end);
		curr[center]->hr = 1 + max(curr[right]->hr, curr[right]->hl);
		curr[center]->hl = 1 + max(curr[left]->hr, curr[left]->hl);
	}
	else if (end-begin < 3)
	{	
		if (left >= begin && left!=center)
		{
			curr[center]->lc = curr[left];
			curr[center]->hl = 1;
			curr[left]->rc = NULL;
			curr[left]->lc = NULL;
		}
		else
		{
			curr[center]->lc = NULL;
		}
		
		if (right <= end && right!=center)
		{
			curr[center]->rc = curr[right];
			curr[center]->hr = 1;
			curr[right]->rc = NULL;
			curr[right]->lc = NULL;
		}
		else
		{
			curr[center]->rc = NULL;
		}
		return;
	}
}

//Function that searches the minimum element from the right subtree of a given node
node* avl_tree::minFromMax(node* initial)
{
	this->search_stack.push(initial);
	if (initial->rc == NULL)
	{
		return NULL;
	}
	node* curr = initial->rc;
	//if (curr->lc == NULL)
	//{
		//stack.push(initial);
	//}
	while (curr->lc != NULL)
	{
		search_stack.push(curr);
		curr = curr->lc;	
	}
	return curr;
}

//Function that searches node from a given node
node* avl_tree::search(node* initial, unsigned int find)
{
	stack <node*> stack;
	node* curr = initial;
	while (true)
	{
		if (curr->id == find)
		{
			this->search_stack = stack;
			return curr;
		}
		else if (curr->rc == NULL && curr->lc == NULL)
	  {
			cout << "ERROR: El nodo no se encuentra en el arbol \n \n";
			return NULL;
	  }
	  else if (curr->id < find)
		{
			stack.push(curr);
			curr = curr->rc;
		}
	  else if (curr->id > find)
		{
			stack.push(curr);
			curr = curr->lc;
		}
	}
}

//Function that searchs node from root
node* avl_tree::searchFromRoot(unsigned int find)
{
	return search(this->root, find);
}

//Function that updates nodes updates after a deletion or balance
void avl_tree::deleteHeightUpdate(node* in)
{
	node* curr = in;
	stack <node*> temp_stack = this->search_stack;
	while(!search_stack.empty())
	{
		if (search_stack.top()->rc != NULL && search_stack.top()->rc->id == curr->id)
		{
			search_stack.top()->hr -= 1;
			if (search_stack.top()->hr < search_stack.top()->hl)
			{
				this->search_stack = temp_stack;
				return;
			}
		}
		else if (search_stack.top()->lc != NULL && search_stack.top()->lc->id == curr->id)
		{
			search_stack.top()->hl -= 1;
			if (search_stack.top()->hl < search_stack.top()->hr)
			{
				this->search_stack = temp_stack;
				return;
			}
		}
		curr = search_stack.top();
		search_stack.pop();
	}
	this->search_stack = temp_stack;
	return;
}

void avl_tree::deleteNode(node* deletedNode)
{	
	if (this->search_stack.empty())
	{
		searchFromRoot(deletedNode->id);
	}
	if (deletedNode == NULL)
	{
		cout << "ERROR: El nodo no existe, no se puede borrar \n \n";
		return;
	}
	else if(deletedNode != NULL){
		if (deletedNode->rc == NULL && deletedNode->lc == NULL)
		{
			node* parent = this->search_stack.top();
			deleteHeightUpdate(deletedNode);
			if (parent == NULL)
			{
				this->root = NULL;
				cout << "Eliminado el último elemento del árbol. El árbol está vacío \n \n";
			}
			else if (parent->id > deletedNode->id)
			{
				parent->lc = NULL;
				parent->hl = 0;
			}
			else if (parent->id < deletedNode->id)
			{
				parent->rc = NULL;
				parent->hr = 0;	
			}
			return;
		}
		else if (deletedNode->rc == NULL && deletedNode->lc != NULL)
		{
			searchFromRoot(deletedNode->lc->id);
			node* temp_pointer = deletedNode->lc;
			deleteHeightUpdate(temp_pointer);
			*deletedNode = *temp_pointer;
			return;
		}
		else if (minFromMax(deletedNode)->rc != NULL)
		{	
			searchFromRoot(deletedNode->id);
			node* temp_pointer = minFromMax(deletedNode);
			node temp = *temp_pointer;
			deleteHeightUpdate(temp_pointer);
			*temp_pointer = *temp_pointer->rc;
			temp.rc = deletedNode->rc;
			temp.lc = deletedNode->lc;
			temp.hr = deletedNode->hr;
			temp.hl = deletedNode->hl;
			*deletedNode = temp;
			return;
		}
		else if (minFromMax(deletedNode)->rc == NULL)
		{ 
			searchFromRoot(deletedNode->id);
			stack <node*> temp_stack = this->search_stack;
			node* temp_pointer = minFromMax(deletedNode);
			node temp = *temp_pointer;
			deleteNode(temp_pointer);
			temp.rc = deletedNode->rc;
			temp.lc = deletedNode->lc;
			temp.hr = deletedNode->hr;
			temp.hl = deletedNode->hl;
			*deletedNode = temp;
			return;
		}
	}
}

//Functions that searchs node from root and deletes it
void avl_tree::deleteNode(unsigned int deletedNumber)
{
	node* node_delete = searchFromRoot(deletedNumber);
	if (node_delete == NULL)
	{
		cout << "ERROR: El nodo no existe, no se puede borrar \n \n";
		return;
	}
	deleteNode(node_delete);
}

//Function that updates the height of the nodes after a insertion
void avl_tree::insertHeightUpdate(node* in)
{
	node* curr = in;
	stack <node*> temp_stack = this->search_stack;
	while(!search_stack.empty())
	{
		if (search_stack.top()->rc != NULL && search_stack.top()->rc->id == curr->id)
		{
			search_stack.top()->hr += 1;
			if (search_stack.top()->hr <= search_stack.top()->hl)
			{
				this->search_stack = temp_stack;
				return;
			}
		}
		else if (search_stack.top()->lc != NULL && search_stack.top()->lc->id == curr->id)
		{
			search_stack.top()->hl += 1;
			if (search_stack.top()->hl <= search_stack.top()->hr)
			{
				this->search_stack = temp_stack;
				return;
			}
		}
		curr = search_stack.top();
		search_stack.pop();
	}
	this->search_stack = temp_stack;
	return;
}

//Function that inserts a given node
void avl_tree::insertNode(node* inserted)
{
	if (inserted == NULL)
	{
		cout << "ERROR: No se puede insertar un nodo nulo \n\n";
		return;
	}
	if (this->root == NULL)
	{
		this->root = inserted;
		return;
	}
	node* curr = this->root;
	stack <node*> stack;
	while (true)
	{
		if (curr->id <= inserted->id)
		{
			stack.push(curr);
			if (curr->rc != NULL)
			{
				curr = curr->rc;
			}
			else
			{
				curr->rc = inserted;
				this->search_stack = stack;
				insertHeightUpdate(inserted);
				return;
			}
		}
		else if (curr->id > inserted->id)
		{
			stack.push(curr);
			if (curr->lc != NULL)
			{
				curr = curr->lc;
			}
			else
			{
				curr->lc = inserted;
				this->search_stack = stack;
				//showstack();
				insertHeightUpdate(inserted);
				return;
			}
		}
	}
}

//Performs an LL rotation on a given node
void avl_tree::rotationLL(node* initial)
{
  node* temp1 = initial->lc;
  node temp2 = *initial;
  temp2.lc = initial->lc->rc;
  *initial = *initial->lc;
  *temp1 = temp2;
  initial->rc = temp1;
  initial->rc->hl = initial->hr;
  initial->hr = 1 + max(initial->rc->hl,initial->rc->hr);
  deleteHeightUpdate(initial);
  return;
}

//Performs an RR rotation on a given node
void avl_tree::rotationRR(node* initial)
{
	node* temp1 = initial->rc;
	node temp2 = *initial;
	temp2.rc = initial->rc->lc;
	*initial = *initial->rc;
	*temp1 = temp2;
	initial->lc = temp1;
	initial->lc->hr = initial->hl;
	initial->hl = 1 + max(initial->lc->hr,initial->lc->hl);
	deleteHeightUpdate(initial);
	return;
}

//Performs an RL rotation on a given node
void avl_tree::rotationRL(node* initial)
{
	node* temp1 = initial->rc->lc;
	node temp2 = *initial;
	node* temp3 = initial->rc;
	initial->rc->lc = initial->rc->lc->rc; 
	temp2.rc = temp1->lc;
	*initial = *temp1;
	*temp1 = temp2;
	initial->lc = temp1;
	initial->rc = temp3;
	initial->lc->hr = initial->hl;
	initial->rc->hl = initial->hr;
	initial->hl = 1 + max(initial->lc->hr,initial->lc->hl);
	initial->hr = 1 + max(initial->rc->hl,initial->rc->hr);
	deleteHeightUpdate(initial);
	return;
}

//Performs an LR rotation on a given node
void avl_tree::rotationLR(node* initial)
{
	node* temp1 = initial->lc->rc;
	node temp2 = *initial;
	node* temp3 = initial->lc;
	initial->lc->rc = initial->lc->rc->lc; 
	temp2.lc = temp1->rc;
	*initial = *temp1;
	*temp1 = temp2;
	initial->rc = temp1;
	initial->lc = temp3;
	initial->lc->hr = initial->hl;
	initial->rc->hl = initial->hr;
	initial->hl = 1 + max(initial->lc->hr,initial->lc->hl);
	initial->hr = 1 + max(initial->rc->hl,initial->rc->hr);
	deleteHeightUpdate(initial);
	return;
}

//Function that prints a node stack
void avl_tree::printstack(stack<node*> stck)
{
    stack <node*> g = stck;
    cout << "Stack: ";
    while (!g.empty())
    {
        cout << g.top()->id << " (" << g.top()->hl << ", " << g.top()->hr << ") ";
        g.pop();
    }
    cout << '\n';
}

//Function that runs the tree stack searching for an inbalace after an insertion or deletion, and if it finds an imbalance, 
//it applies the respective rotation to balance the tree
void avl_tree::balance()
{
	while (!this->search_stack.empty()){
		if (this->search_stack.top()->hr - this->search_stack.top()->hl >= 2)
		{
			if (this->search_stack.top()->rc->hr - this->search_stack.top()->rc->hl >= 1)
			{
				rotationRR(this->search_stack.top());
			}
			else if (this->search_stack.top()->rc->hr - this->search_stack.top()->rc->hl <= -1)
			{
				rotationRL(this->search_stack.top());
			}
			else
			{
				cout << "ERROR: Hay un error con las alturas\n";
			}
		}
		else if (this->search_stack.top()->hl - this->search_stack.top()->hr >= 2)
		{
			if (this->search_stack.top()->lc->hl - this->search_stack.top()->lc->hr >= 1)
			{
				rotationLL(this->search_stack.top());
			}
			else if (this->search_stack.top()->lc->hl - this->search_stack.top()->lc->hr <= -1)
			{
				rotationLR(this->search_stack.top());
			}
			else
			{
				cout << "ERROR: Hay un error con las alturas\n";
			}
		}
		else
		{
			this->search_stack.pop();
		}
	}
}

//Functions that inserts a node, then balances the tree
void avl_tree::avl_insert(node* insert)
{
	insertNode(insert);
	balance();
}

//Functions that deletes a node, then balances the tree
void avl_tree::avl_delete(node* deleted)
{
	deleteNode(deleted);
	balance();
}

//Functions that searchs node from root and deletes it. Then balances the tree
void avl_tree::avl_delete(unsigned int deletedNumber)
{
	node* node_delete = searchFromRoot(deletedNumber);
	if (node_delete == NULL)
	{
		cout << "ERROR: El nodo no existe, no se puede borrar \n \n";
		return;
	}
	avl_delete(node_delete);
}

node* avl_tree::get_maxnode()
{
	node* curr = this->root;
	while (curr->rc != NULL)
	{
		curr = curr->rc;
	} 
	return curr;
}

node* avl_tree::get_minnode()
{
	node* curr = this->root;
	while (curr->lc != NULL)
	{
		curr = curr->lc;
	} 
	return curr;
}
